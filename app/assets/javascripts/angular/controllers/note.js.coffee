App.controller 'NotesCtrl', ['$scope', 'Notes', 'Note', ($scope, Notes, Note) ->
  selectedNotes = {}
  $scope.notes = Notes.query()

  $scope.showNote = (id) ->
    if selectedNotes[id]
      $scope.selectedNote = selectedNotes[id]
    else
      $scope.selectedNote = Note.get({id: id})
      selectedNotes[id] = $scope.selectedNote
    $scope.editingNote = null

  $scope.newNote = ->
    note = new Notes({title: $scope.title, body: $scope.text})
    note.$save(()->
      $scope.notes = Notes.query()
      $scope.editingNote = null
      $scope.newNote = null
    )


  $scope.deleteNote = (id)->
    Note.delete({id: id})
    $scope.notes = Notes.query()

  $scope.editNote = (id) ->
    $scope.editingNote = true
    if id and $scope.selectedNote
      $scope.title = $scope.selectedNote.title
      $scope.text = $scope.selectedNote.body
      $scope.id = $scope.selectedNote.id
      $scope.saveNote = (id) ->
        if id
          note = new Note({title: $scope.title, body: $scope.text})
          note.id = id
          note.$save((note, response)->
            $scope.selectedNote = note
            $scope.editingNote = null
            selectedNotes[id] = null
            $scope.notes = Notes.query()
          )
    else
      $scope.title = null
      $scope.text = null
      $scope.id = null
      $scope.saveNote = ()->
        note = new Notes({title: $scope.title, body: $scope.text})
        note.$save((note)->
          $scope.editingNote = null
          $scope.selectedNote = note
          $scope.notes = Notes.query()
        )

    $scope.selectedNote = null
  return
]
