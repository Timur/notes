App.factory 'Notes', ['$resource', ($resource) ->
  $resource '/api/notes'
]

App.factory 'Note', ['$resource', ($resource) ->
  $resource '/api/note/:id', id: '@id'
]